package sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class BasicQuery {

    private static final String DATASOURCE_URL = "jdbc:oracle:thin:@//localhost:1521/ORCLPDB1.localdomain";
    private static final String USER_PASS      = "hr";

    public static void main(String[] args) throws SQLException {

        try (Connection conn = DriverManager.getConnection(DATASOURCE_URL, USER_PASS, USER_PASS)) {

            Statement stmt = conn.createStatement();

            ResultSet rs = stmt.executeQuery("select * from EMPLOYEESS");
            while (rs.next())
                System.out.println(rs.getInt(1) + "  " + rs.getString(2) + "  " + rs.getString(3));
        }
    }
}
