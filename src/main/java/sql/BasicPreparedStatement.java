package sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

public class BasicPreparedStatement {

    private static final String DATASOURCE_URL = "jdbc:oracle:thin:@//localhost:1521/ORCLPDB1.localdomain";
    private static final String USER_PASS      = "hr";

    public static void main(String[] args) throws SQLException {

        try (Connection conn = DriverManager.getConnection(DATASOURCE_URL, USER_PASS, USER_PASS)) {
            //this is a regular statement
            Statement statement = conn.createStatement();

            //this is a prepared statement
            PreparedStatement preparedStatement = conn.prepareStatement("insert into REGIONS values(?,?)");

            addEndavaTowerRegion(preparedStatement);

        }
    }

    private static void addEndavaTowerRegion(PreparedStatement preparedStatement) throws SQLException {
        //PreparedStatement is a sub-interface of Statement. It is used to execute parameterized query.

        preparedStatement.setInt(1, 6);
        preparedStatement.setString(2, "ENDAVA Tower");

        System.out.println(preparedStatement.executeUpdate() + " records inserted");

    }

}
