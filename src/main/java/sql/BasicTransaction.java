package sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class BasicTransaction {

    private static final String DATASOURCE_URL = "jdbc:oracle:thin:@//localhost:1521/ORCLPDB1.localdomain";
    private static final String USER_PASS      = "hr";

    public static void main(String[] args) throws SQLException {

        try (Connection conn = DriverManager.getConnection(DATASOURCE_URL, USER_PASS, USER_PASS)) {
            conn.setAutoCommit(false);//Oracle has this on by default

            PreparedStatement preparedStatement = conn.prepareStatement("insert into REGIONS values(?,?)");

            preparedStatement.setInt(1, 17);
            preparedStatement.setString(2, "DO NOT INSERT ME ANYWHERE");

            System.out.println(preparedStatement.executeUpdate() + " records inserted");

            conn.rollback();
            conn.commit();
        }
    }

}
