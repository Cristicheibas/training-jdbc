package sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class BasicResultSet {

    private static final String DATASOURCE_URL = "jdbc:oracle:thin:@//localhost:1521/ORCLPDB1.localdomain";
    private static final String USER_PASS      = "hr";

    public static void main(String[] args) throws SQLException {

        try (Connection conn = DriverManager.getConnection(DATASOURCE_URL, USER_PASS, USER_PASS)) {
            //selectEmployeesWithJ(conn);
            selectOnlyFirstEmployeeWithJ(conn);
        }
    }

    private static void selectEmployeesWithJ(Connection conn) throws SQLException {
        Statement statement = conn.createStatement();
        PreparedStatement ps = conn.prepareStatement("select * from EMPLOYEES e where e.first_name LIKE ?");

        ps.setString(1, "%J%");

        //TODO: let's force a syntax mistake here to see how Java behaves?
        ResultSet rs = ps.executeQuery();
        while (rs.next())
            System.out.println(rs.getInt(1) + "  " + rs.getString(2) + "  " + rs.getString(3));
    }

    private static void selectOnlyFirstEmployeeWithJ(Connection conn) throws SQLException {
        Statement statement = conn.createStatement();

        //TODO: let's debug the resultSet and see what is it
        ResultSet rs = statement.executeQuery("select * from EMPLOYEES e where e.first_name LIKE '%J%'");
        rs.next();
        System.out.println(rs.getInt(1) + "  " + rs.getString(2) + "  " + rs.getString(3));
    }
\
}
