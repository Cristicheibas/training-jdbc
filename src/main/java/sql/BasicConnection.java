package sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class BasicConnection {

    private static final String DATASOURCE_URL = "jdbc:oracle:thin:@//localhost:1521/ORCLPDB1.localdomain";
    private static final String USER_PASS = "hr";

    public static void main(String[] args) throws SQLException {
        //refactor into try catch block

        try (Connection conn = DriverManager.getConnection(DATASOURCE_URL, USER_PASS, USER_PASS)) {
            conn.getSchema();
            conn.isReadOnly();
        }
    }
}
